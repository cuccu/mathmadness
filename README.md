# MathMadness

A Clojure library for math expressions parsing.

## Usage

;; First, parse your function

(def func (to-func "sin(x)"))

;;=> #'mathmadness.core/func


;; Then evaluate it

(to-val func 0)

;;=> 0.0


(to-val func (/ (Math/PI) 2))

;;=> 1.0


;; Apply your function to a range of x

(to-vals func (range 0 1 0.2))

;;=> (0.0 0.19866933079506122 0.3894183423086505 0.5646424733950355 0.7173560908995228)


;; Calcolate the integral value (approximation)

(to-integral func 0 3.14159)

;;=> 1.9999999540411524


## Supported unary functions are:

sin, cos, log, sqrt, cbrt, tan, abs
asin, acos, atan, sinh, cosh, tanh

## Supported binary functions are:

+, -, *, /, ^			
		
## Supported constants are:

pi, e			
	
## Examples:

sin(x)

cos(x^pi)

tan(x*cos(x))/(log(x)-2)
