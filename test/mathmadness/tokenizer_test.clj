(ns mathmadness.tokenizer-test
  (:require [clojure.test :refer :all]
            [mathmadness.tokenizer :refer :all]
            [mathmadness.utils :refer :all]))

(deftest x-tests
  (testing 
    "x? tests"
    (is (false? (x? nil)) "should return false on nil")
    (is (false? (x? "")) "should return false on empty")
    (is (false? (x? "abc")) "should return false on abc")
    (is (false? (x? "abcx")) "should return false on abcx")
    (is (true? (x? "xabc")) "should return true on xabc")
    (is (true? (x? "x")) "should return true on x")))

(deftest e-tests
  (testing 
    "e? tests"
    (is (false? (e? nil)) "should return false on nil")
    (is (false? (e? "")) "should return false on empty")
    (is (false? (e? "abc")) "should return false on abc")
    (is (false? (e? "abce")) "should return false on abce")
    (is (true? (e? "eabc")) "should return true on eabc")
    (is (true? (e? "e")) "should return true on e")))

(deftest pi-tests
  (testing 
    "pi? tests"
    (is (false? (pi? nil)) "should return false on nil")
    (is (false? (pi? "")) "should return false on empty")
    (is (false? (pi? "abc")) "should return false on abc")
    (is (false? (pi? "abcpi")) "should return false on abcpi")
    (is (true? (pi? "piabc")) "should return true on piabc")
    (is (true? (pi? "pi")) "should return true on pi")))

(deftest plus-tests
  (testing 
    "plus? tests"
    (is (false? (plus? nil)) "should return false on nil")
    (is (false? (plus? "")) "should return false on empty")
    (is (false? (plus? "abc")) "should return false on abc")
    (is (false? (plus? "abc+")) "should return false on abc+")
    (is (true? (plus? "+abc")) "should return true on +abc")
    (is (true? (plus? "+")) "should return true on +")))

(deftest pow-tests
  (testing 
    "pow? tests"
    (is (false? (pow? nil)) "should return false on nil")
    (is (false? (pow? "")) "should return false on empty")
    (is (false? (pow? "abc")) "should return false on abc")
    (is (false? (pow? "abc^")) "should return false on abc^")
    (is (true? (pow? "^abc")) "should return true on ^abc")
    (is (true? (pow? "^")) "should return true on ^")))

(deftest minus-tests
  (testing 
    "minus? tests"
    (is (false? (minus? nil)) "should return false on nil")
    (is (false? (minus? "")) "should return false on empty")
    (is (false? (minus? "abc")) "should return false on abc")
    (is (false? (minus? "abc-")) "should return false on abc-")
    (is (true? (minus? "-abc")) "should return true on -abc")
    (is (true? (minus? "-")) "should return true on -")))

(deftest times-tests
  (testing 
    "times? tests"
    (is (false? (times? nil)) "should return false on nil")
    (is (false? (times? "")) "should return false on empty")
    (is (false? (times? "abc")) "should return false on abc")
    (is (false? (times? "abc*")) "should return false on abc*")
    (is (true? (times? "*abc")) "should return true on *abc")
    (is (true? (times? "*")) "should return true on *")))

(deftest divide-tests
  (testing 
    "divide? tests"
    (is (false? (divide? nil)) "should return false on nil")
    (is (false? (divide? "")) "should return false on empty")
    (is (false? (divide? "abc")) "should return false on abc")
    (is (false? (divide? "abc/")) "should return false on abc/")
    (is (true? (divide? "/abc")) "should return true on /abc")
    (is (true? (divide? "/")) "should return true on /")))

(deftest open-tests
  (testing 
    "open? tests"
    (is (false? (open? nil)) "should return false on nil")
    (is (false? (open? "")) "should return false on empty")
    (is (false? (open? "abc")) "should return false on abc")
    (is (false? (open? "abc(")) "should return false on anc(")
    (is (true? (open? "(abc")) "should return true on (abc")
    (is (true? (open? "(")) "should return true on (")))

(deftest close-tests
  (testing 
    "close? tests"
    (is (false? (close? nil)) "should return false on nil")
    (is (false? (close? "")) "should return false on empty")
    (is (false? (close? "abc")) "should return false on abc")
    (is (false? (close? "abc)")) "should return false on abc)")
    (is (true? (close? ")abc")) "should return true on )abc")
    (is (true? (close? ")")) "should return true on )")))

(deftest sin-tests
  (testing 
    "sin? tests"
    (is (false? (sin? nil)) "should return false on nil")
    (is (false? (sin? "")) "should return false on empty")
    (is (false? (sin? "abc")) "should return false on abc")
    (is (false? (sin? "abcsin")) "should return false on abcsin")
    (is (true? (sin? "sinabc")) "should return true on sinabc")
    (is (true? (sin? "sin")) "should return true on sin")))

(deftest sinh-tests
  (testing 
    "sinh? tests"
    (is (false? (sinh? nil)) "should return false on nil")
    (is (false? (sinh? "")) "should return false on empty")
    (is (false? (sinh? "abc")) "should return false on abc")
    (is (false? (sinh? "abcsinh")) "should return false on abcsinh")
    (is (true? (sinh? "sinhabc")) "should return true on sinhabc")
    (is (true? (sinh? "sinh")) "should return true on sinh")))

(deftest asin-tests
  (testing 
    "asin? tests"
    (is (false? (asin? nil)) "should return false on nil")
    (is (false? (asin? "")) "should return false on empty")
    (is (false? (asin? "abc")) "should return false on abc")
    (is (false? (asin? "abcasin")) "should return false on abcasin")
    (is (true? (asin? "asinabc")) "should return true on asinabc")
    (is (true? (asin? "asin")) "should return true on asin")))

(deftest cos-tests
  (testing 
    "cos? tests"
    (is (false? (cos? nil)) "should return false on nil")
    (is (false? (cos? "")) "should return false on empty")
    (is (false? (cos? "abc")) "should return false on abc")
    (is (false? (cos? "abccos")) "should return false on abccos")
    (is (true? (cos? "cosabc")) "should return true on cosabc")
    (is (true? (cos? "cos")) "should return true on cos")))

(deftest cosh-tests
  (testing 
    "cosh? tests"
    (is (false? (cosh? nil)) "should return false on nil")
    (is (false? (cosh? "")) "should return false on empty")
    (is (false? (cosh? "abc")) "should return false on abc")
    (is (false? (cosh? "abccosh")) "should return false on abccosh")
    (is (true? (cosh? "coshabc")) "should return true on coshabc")
    (is (true? (cosh? "cosh")) "should return true on cosh")))

(deftest acos-tests
  (testing 
    "acos? tests"
    (is (false? (acos? nil)) "should return false on nil")
    (is (false? (acos? "")) "should return false on empty")
    (is (false? (acos? "abc")) "should return false on abc")
    (is (false? (acos? "abcacos")) "should return false on abcacos")
    (is (true? (acos? "acosabc")) "should return true on acosabc")
    (is (true? (acos? "acos")) "should return true on acos")))

(deftest tan-tests
  (testing 
    "tan? tests"
    (is (false? (tan? nil)) "should return false on nil")
    (is (false? (tan? "")) "should return false on empty")
    (is (false? (tan? "abc")) "should return false on abc")
    (is (false? (tan? "abctan")) "should return false on abctan")
    (is (true? (tan? "tanabc")) "should return true on tanabc")
    (is (true? (tan? "tan")) "should return true on tan")))

(deftest tanh-tests
  (testing 
    "tanh? tests"
    (is (false? (tanh? nil)) "should return false on nil")
    (is (false? (tanh? "")) "should return false on empty")
    (is (false? (tanh? "abc")) "should return false on abc")
    (is (false? (tanh? "abctanh")) "should return false on abctanh")
    (is (true? (tanh? "tanhabc")) "should return true on tanhabc")
    (is (true? (tanh? "tanh")) "should return true on tanh")))

(deftest atan-tests
  (testing 
    "tan? tests"
    (is (false? (atan? nil)) "should return false on nil")
    (is (false? (atan? "")) "should return false on empty")
    (is (false? (atan? "abc")) "should return false on abc")
    (is (false? (atan? "abcatan")) "should return false on abcatan")
    (is (true? (atan? "atanabc")) "should return true on atanabc")
    (is (true? (atan? "atan")) "should return true on atan")))

(deftest log-tests
  (testing 
    "log? tests"
    (is (false? (log? nil)) "should return false on nil")
    (is (false? (log? "")) "should return false on empty")
    (is (false? (log? "abc")) "should return false on abc")
    (is (false? (log? "abclog")) "should return false on abclog")
    (is (true? (log? "logabc")) "should return true on logabc")
    (is (true? (log? "log")) "should return true on log")))

(deftest sqrt-tests
  (testing 
    "sqrt? tests"
    (is (false? (sqrt? nil)) "should return false on nil")
    (is (false? (sqrt? "")) "should return false on empty")
    (is (false? (sqrt? "abc")) "should return false on abc")
    (is (false? (sqrt? "abcsqrt")) "should return false on abcsqrt")
    (is (true? (sqrt? "sqrtabc")) "should return true on sqrtabc")
    (is (true? (sqrt? "sqrt")) "should return true on sqrt")))

(deftest cbrt-tests
  (testing 
    "cbrt? tests"
    (is (false? (cbrt? nil)) "should return false on nil")
    (is (false? (cbrt? "")) "should return false on empty")
    (is (false? (cbrt? "abc")) "should return false on abc")
    (is (false? (cbrt? "abccbrt")) "should return false on abccbrt")
    (is (true? (cbrt? "cbrtabc")) "should return true on cbrtabc")
    (is (true? (cbrt? "cbrt")) "should return true on cbrt")))

(deftest abs-tests
  (testing 
    "abs? tests"
    (is (false? (abs? nil)) "should return false on nil")
    (is (false? (abs? "")) "should return false on empty")
    (is (false? (abs? "abc")) "should return false on abc")
    (is (false? (abs? "abcabs")) "should return false on abcabs")
    (is (true? (abs? "absabc")) "should return true on absabc")
    (is (true? (abs? "abs")) "should return true on abs")))

(deftest numb-tests
  (testing 
    "numb? tests"
    (is (false? (numb? nil)) "should return false on nil")
    (is (false? (numb? "")) "should return false on empty")
    (is (false? (numb? "abc")) "should return false on abc")
    (is (false? (numb? "abc23")) "should return false on abc23")
    (is (true? (numb? "2abc")) "should return true on 2abc")
    (is (true? (numb? "3")) "should return true on 3")))

(deftest numbs-tests
  (testing 
    "numbs tests"
    (is (empty? (numbs nil)) "should return empty on nil")
    (is (empty? (numbs "")) "should return empty on empty")
    (is (empty? (numbs "abc")) "should return empty on abc")
    (is (empty? (numbs "abc234")) "should return empty on abc234")
    (is (= "234" (numbs "234abc")) "should return 234 on 234abc")
    (is (= "345" (numbs "345adb66")) "should return 345 on 345adb66")
    (is (= ".1" (numbs ".1")) "should return .1 on .1")
    (is (= "1.2" (numbs "1.2")) "should return 1.2 on 1.2")
    (is (= "1.2." (numbs "1.2.")) "should return 1.2. on 1.2.")
    (is (= "1.2.3" (numbs "1.2.3")) "should return 1.2.3 on 1.2.3")))

(deftest rmspcs-tests
  (testing 
    "rmspcs tests"
    (is (nil? (rmspcs nil)) "should return nil on nil")
    (is (empty? (rmspcs "")) "should return empty on empty string")
    (is (= "abc" (rmspcs "abc")) "should not change a string without spaces")
    (is (= "abcdef" (rmspcs " abc def ")) "should remove spaces")))

(deftest tokenize-tests
  (testing 
    "tokenize tests"
    (is (= ["(" "(" "(" "(" "2" ")" ")" ")" ")"] (tokenize "2")) "should tokenize 2")
    (is (= ["(" "(" "(" "(" ".2" ")" ")" ")" ")"] (tokenize ".2")) "should tokenize .2")
    (is (= ["(" "(" "(" "(" "1.2" ")" ")" ")" ")"] (tokenize "1.2")) "should tokenize 1.2")
    (is (thrown? Exception (["(" "(" "(" "(" "1.2." ")" ")" ")" ")"] (tokenize "1.2.")) "should thrown on 1.2."))
    (is (thrown? Exception (["(" "(" "(" "(" "1.2.3" ")" ")" ")" ")"] (tokenize "1.2.3")) "should thrown on 1.2.3"))
    (is (= ["(" "(" "(" "(" "e" ")" ")" ")" ")"] (tokenize "e")) "should tokenize e")
    (is (= ["(" "(" "(" "(" "e" ")" ")" ")" ")"] (tokenize "E")) "should tokenize E")
    (is (= ["(" "(" "(" "(" "pi" ")" ")" ")" ")"] (tokenize "pi")) "should tokenize pi")
    (is (= ["(" "(" "(" "(" "pi" ")" ")" ")" ")"] (tokenize "PI")) "should tokenize PI")
    (is (= ["(" "(" "(" "(" "x" ")" ")" ")" ")"] (tokenize "x")) "should tokenize x")
    (is (= ["(" "(" "(" "(" "2" ")" ")" ")" "+" "(" "(" "(" "3" ")" ")" ")" ")"] (tokenize "2 + 3")) "should tokenize +")
    (is (= ["(" "(" "(" "(" "2" ")" ")" ")" "-" "(" "(" "(" "3" ")" ")" ")" ")"] (tokenize "2 - 3")) "should tokenize -")
    (is (= ["(" "(" "(" "(" "2" ")" ")" "*" "(" "(" "3" ")" ")" ")" ")"] (tokenize "2 * 3")) "should tokenize *")
    (is (= ["(" "(" "(" "(" "2" ")" ")" "/" "(" "(" "3" ")" ")" ")" ")"] (tokenize "2 / 3")) "should tokenize /")
    (is (= ["(" "(" "(" "(" "1" ")" ")" ")" "+" "(" "(" "(" "2" ")" ")" "*" "(" "(" "3" ")" ")" ")" ")"] (tokenize "1+2*3")) "should tokenize 1+2*3")
    (is (= ["(" "(" "(" "(" "1" ")" ")" "*" "(" "(" "2" ")" ")" ")" "+" "(" "(" "(" "3" ")" ")" ")" ")"] (tokenize "1*2+3")) "should tokenize 1*2+3")
    (is (= ["(" "(" "(" "(" "sin" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "sin(x)")) "should tokenize sin(x)")
    (is (= ["(" "(" "(" "(" "asin" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "asin(x)")) "should tokenize asin(x)")
    (is (= ["(" "(" "(" "(" "sinh" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "sinh(x)")) "should tokenize sinh(x)")
    (is (= ["(" "(" "(" "(" "cos" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "cos(x)")) "should tokenize cos(x)")
    (is (= ["(" "(" "(" "(" "acos" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "acos(x)")) "should tokenize acos(x)")
    (is (= ["(" "(" "(" "(" "cosh" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "cosh(x)")) "should tokenize cosh(x)")
    (is (= ["(" "(" "(" "(" "tan" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "tan(x)")) "should tokenize tan(x)")
    (is (= ["(" "(" "(" "(" "atan" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "atan(x)")) "should tokenize atan(x)")
    (is (= ["(" "(" "(" "(" "tanh" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "tanh(x)")) "should tokenize tanh(x)")
    (is (= ["(" "(" "(" "(" "log" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "log(x)")) "should tokenize log(x)")
    (is (= ["(" "(" "(" "(" "abs" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "abs(x)")) "should tokenize abs(x)")
    (is (= ["(" "(" "(" "(" "sqrt" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "sqrt(x)")) "should tokenize sqrt(x)")
    (is (= ["(" "(" "(" "(" "cbrt" "(" "(" "(" "(" "x" ")" ")" ")" ")" ")" ")" ")" ")"] (tokenize "cbrt(x)")) "should tokenize cbrt(x)")
    (is (= ["(" "(" "(" "(" "2" ")" "^" "(" "3" ")" ")" ")" ")"] (tokenize "2^3")) "should tokenize 2^3")))
    
  

















  
  