(ns mathmadness.parser-test
  (:require [clojure.test :refer :all]
            [mathmadness.parser :refer :all]
            [mathmadness.tokenizer :refer :all]))

(deftest value?-test
  (testing 
    "value?"
    (is (false? (value? nil)) "should return false on nil")
    (is (false? (value? [])) "should return false on empty")
    (is (false? (value? ["+"])) "should return false on operation")
    (is (false? (value? ["sin"])) "should return false on sin")
    (is (false? (value? ["("])) "should return false on open")
    (is (true? (value? ["23"])) "should return true on number")
    (is (true? (value? [".3"])) "should return true on number")
    (is (true? (value? ["2.3"])) "should return true on number")))

(deftest ope?-test
  (testing 
    "ope?"
    (is (false? (ope? nil)) "should return false on nil")
    (is (false? (ope? [nil])) "should return false on nil")
    (is (false? (ope? [])) "should return false on empty")
    (is (false? (ope? [""])) "should return false on empty")
    (is (false? (ope? ["abc"])) "should return false on abc")
    (is (false? (ope? ["sin"])) "should return false on sin")
    (is (false? (ope? ["asin"])) "should return false on asin")
    (is (false? (ope? ["sinh"])) "should return false on sinh")
    (is (false? (ope? ["cos"])) "should return false on cos")
    (is (false? (ope? ["acos"])) "should return false on acos")
    (is (false? (ope? ["cosh"])) "should return false on cosh")
    (is (false? (ope? ["tan"])) "should return false on cos")
    (is (false? (ope? ["atan"])) "should return false on atan")
    (is (false? (ope? ["tanh"])) "should return false on tanh")
    (is (false? (ope? ["log"])) "should return false on log")
    (is (false? (ope? ["sqrt"])) "should return false on sqrt")
    (is (false? (ope? ["cbrt"])) "should return false on cbrt")
    (is (false? (ope? ["23"])) "should return false on number")
    (is (false? (ope? ["x"])) "should return false on x")
    (is (false? (ope? ["pi"])) "should return false on pi")
    (is (false? (ope? ["e"])) "should return false on e")
    (is (false? (ope? ["("])) "should return false on open")
    (is (false? (ope? [")"])) "should return false on close")
    (is (true? (ope? ["+"])) "should return true on plus")
    (is (true? (ope? ["-"])) "should return true on minus")
    (is (true? (ope? ["*"])) "should return true on times")
    (is (true? (ope? ["/"])) "should return true on divide")
    (is (true? (ope? ["^"])) "should return true on pow")))

(deftest unary?-test
  (testing 
    "unary?"
    (is (false? (unary? nil)) "should return false on nil")
    (is (false? (unary? [nil])) "should return false on nil")
    (is (false? (unary? [])) "should return false on nil")
    (is (false? (unary? [""])) "should return false on empty")
    (is (false? (unary? ["abc"])) "should return false on abc")
    (is (false? (unary? ["sinabc"])) "should return false on sinabc")
    (is (false? (unary? ["abcsin"])) "should return false on abcsin")
    (is (true? (unary? ["sin"])) "should return true on sin")
    (is (true? (unary? ["asin"])) "should return true on asin")
    (is (true? (unary? ["sinh"])) "should return true on sinh")
    (is (true? (unary? ["cos"])) "should return true on cos")
    (is (true? (unary? ["acos"])) "should return true on acos")
    (is (true? (unary? ["cosh"])) "should return true on cosh")
    (is (true? (unary? ["tan"])) "should return true on tan")
    (is (true? (unary? ["atan"])) "should return true on atan")
    (is (true? (unary? ["tanh"])) "should return true on tanh")
    (is (true? (unary? ["log"])) "should return true on log")
    (is (true? (unary? ["sqrt"])) "should return true on sqrt")
    (is (true? (unary? ["cbrt"])) "should return true on cbrt")))

(deftest ope-value-test
  (testing 
    "ope-value"
    (is (nil? ((ope-value nil))) "should return f that returns nil on nil")
    (is (empty? ((ope-value ""))) "should return f that returns empty on empty")
    (is (= "abc" ((ope-value "abc"))) "should return f that returns str on str")
    (is (= 23 ((ope-value 23))) "should return f that returns 23 on 23")
    (is (= 0.3 ((ope-value 0.3))) "should return f that returns 23 on .3")
    (is (= 2.3 ((ope-value 2.3))) "should return f that returns 23 on 2.3")))

(deftest ope-add-test
  (testing 
    "ope-add"
    (is (= 8 ((ope-add (fn [] 3) (fn [] 5)))) "should return f that returns add")))

(deftest ope-sub-test
  (testing 
    "ope-sub"
    (is (= 2 ((ope-sub (fn [] 5) (fn [] 3)))) "should return f that returns sub")))

(deftest ope-mult-test
  (testing 
    "ope-mult"
    (is (= 15 ((ope-mult (fn [] 5) (fn [] 3)))) "should return f that returns mult")))

(deftest ope-negative-test
  (testing 
    "ope-negative"
    (is (= -2 ((ope-negative (fn [] 2)))) "should return f that returns negative")
    (is (= 2 ((ope-negative (fn [] -2)))) "should return f that returns negative")))

(deftest ope-division-test
  (testing 
    "ope-division"
    (is (= 2 ((ope-division (fn [] 6) (fn [] 3)))) "should return f that returns division")
    (is (Double/isNaN ((ope-division (fn [] 6) (fn [] 0)))) "should return f that returns NaN")
    (is (Double/isNaN ((ope-division (fn [] 6) (fn [] (Math/sin 0))))) "should return f that returns NaN")))

(deftest ope-pow-test
  (testing 
    "ope-pow"
    (is (= 27.0 ((ope-pow (fn [] 3) (fn [] 3)))) "should return f that returns pow")
    (is (= 9.0 ((ope-pow (fn [] 3) (fn [] 2)))) "should return f that returns pow")
    (is (= 3.0 ((ope-pow (fn [] 3) (fn [] 1)))) "should return f that returns pow")
    (is (= 1.0 ((ope-pow (fn [] 3) (fn [] 0)))) "should return f that returns pow")))

(deftest ope-sin-test
  (testing 
    "ope-sin"
    (is (= (Math/sin 3) ((ope-sin (fn [] 3)))) "should return f that returns sin(3)")))

(deftest ope-asin-test
  (testing 
    "ope-asin"
    (is (= (Math/asin 0.5) ((ope-asin (fn [] 0.5)))) "should return f that returns asin(0.5)")))

(deftest ope-sinh-test
  (testing 
    "ope-sinh"
    (is (= (Math/sinh 3) ((ope-sinh (fn [] 3)))) "should return f that returns sinh(3)")))

(deftest ope-cos-test
  (testing 
    "ope-cos"
    (is (= (Math/cos 3) ((ope-cos (fn [] 3)))) "should return f that returns cos(3)")))

(deftest ope-acos-test
  (testing 
    "ope-acos"
    (is (= (Math/acos 0.5) ((ope-acos (fn [] 0.5)))) "should return f that returns acos(0.5)")))

(deftest ope-cosh-test
  (testing 
    "ope-cosh"
    (is (= (Math/cosh 3) ((ope-cosh (fn [] 3)))) "should return f that returns cosh(3)")))

(deftest ope-tan-test
  (testing 
    "ope-tan"
    (is (= (Math/tan 3) ((ope-tan (fn [] 3)))) "should return f that returns tan(3)")))

(deftest ope-atan-test
  (testing 
    "ope-atan"
    (is (= (Math/atan 0.5) ((ope-atan (fn [] 0.5)))) "should return f that returns atan(0.5)")))

(deftest ope-tanh-test
  (testing 
    "ope-tanh"
    (is (= (Math/tanh 3) ((ope-tanh (fn [] 3)))) "should return f that returns tanh(3)")))

(deftest ope-log-test
  (testing 
    "ope-log"
    (is (= (Math/log 3) ((ope-log (fn [] 3)))) "should return f that returns log(3)")))

(deftest ope-sqrt-test
  (testing 
    "ope-sqrt"
    (is (= (Math/sqrt 3) ((ope-sqrt (fn [] 3)))) "should return f that returns sqrt(3)")))

(deftest ope-cbrt-test
  (testing 
    "ope-cbrt"
    (is (= (Math/cbrt 3) ((ope-cbrt (fn [] 3)))) "should return f that returns cbrt(3)")))

(deftest ope-abs-test
  (testing 
    "ope-abs"
    (is (= (Math/abs 3) ((ope-abs (fn [] -3)))) "should return f that returns abs(-3)")))

(deftest ope-test
  (testing 
    "ope"
    (is (= 5 ((ope "+" (fn [] 2) (fn [] 3)))) "should return plus-ope")
    (is (= 5 ((ope "-" (fn [] 7) (fn [] 2)))) "should return minus-ope")
    (is (= 6 ((ope "*" (fn [] 2) (fn [] 3)))) "should return times-ope")
    (is (= 2 ((ope "/" (fn [] 8) (fn [] 4)))) "should return divide-ope")
    (is (= 8.0 ((ope "^" (fn [] 2) (fn [] 3)))) "should return pow-ope")))

(deftest unary-test
  (testing 
    "unary"
    (is (= (Math/sin 3) ((unary "sin" (fn [] 3)))) "should return sin-ope")
    (is (= (Math/asin 0.5) ((unary "asin" (fn [] 0.5)))) "should return asin-ope")
    (is (= (Math/sinh 3) ((unary "sinh" (fn [] 3)))) "should return sinh-ope")
    (is (= (Math/cos 2) ((unary "cos" (fn [] 2)))) "should return cos-ope")
    (is (= (Math/acos 0.5) ((unary "acos" (fn [] 0.5)))) "should return acos-ope")
    (is (= (Math/cosh 2) ((unary "cosh" (fn [] 2)))) "should return cosh-ope")
    (is (= (Math/tan 2) ((unary "tan" (fn [] 2)))) "should return tan-ope")
    (is (= (Math/atan 0.5) ((unary "atan" (fn [] 0.5)))) "should return atan-ope")
    (is (= (Math/tanh 2) ((unary "tanh" (fn [] 2)))) "should return tanh-ope")
    (is (= (Math/log 3) ((unary "log" (fn [] 3)))) "should return log-ope")
    (is (= (Math/sqrt 4) ((unary "sqrt" (fn [] 4)))) "should return sqrt-ope")
    (is (= (Math/cbrt 4) ((unary "cbrt" (fn [] 4)))) "should return cbrt-ope")
    (is (= (Math/abs 4) ((unary "abs" (fn [] -4)))) "should return abs-ope")))

(deftest parse-value-test
  (testing 
    "parse value"
    (is (= 2.0 ((parse ["2"]))) "should parse value")))

(deftest parse-x-test
  (testing 
    "parse x"
    (is (= 1 ((parse ["x"]))) "should parse x")))

(deftest parse-e-test
  (testing 
    "parse e"
    (is (= (Math/E) ((parse ["e"]))) "should parse e")))

(deftest parse-pi-test
  (testing 
    "parse pi"
    (is (= (Math/PI) ((parse ["pi"]))) "should parse pi")))

(deftest parse-negative-test
  (testing 
    "parse negative"
    (is (= -1 ((parse ["-" "x"]))) "should parse -x")
    (is (= -2.0 ((parse ["-" "2"]))) "should parse -2")
    (is (= -2.0 ((parse ["-" "(" "2" ")"]))) "should parse -(2)")
    (is (= -2.0 ((parse ["(" "-" "2" ")"]))) "should parse (-2)")
    (is (= 2.0 ((parse ["-" "-" "2"]))) "should parse --2")
    (is (= (- Math/E) ((parse ["-" "e"]))) "should parse -e")
    (is (= (- Math/PI) ((parse ["-" "pi"]))) "should parse -pi")
    (is (= (- (Math/sin 1)) ((parse ["-" "sin" "(" "x" ")"]))) "should parse -sin(x)")
    (is (= (Math/sin (- 1)) ((parse ["sin" "(" "-" "x" ")"]))) "should parse sin(-x)")
    (is (= -6.0 ((parse ["2" "*" "-" "3"]))) "should parse 2*-3")))

(deftest parse-sin-test
  (testing 
    "parse sin"
    (is (thrown? Exception (parse ["sin"])) "should throw exception")
    (is (thrown? Exception (parse ["sin" "("])) "should throw exception")
    (is (thrown? Exception (parse ["sin" "(" "3"])) "should throw exception")
    (is (= (Math/sin 3) ((parse ["sin" "(" "3" ")"]))) "should parse sin(3)")
    (is (= (Math/sin 1) ((parse ["sin" "(" "x" ")"]))) "should parse sin(x)")))

(deftest parse-asin-test
  (testing 
    "parse asin"
    (is (thrown? Exception (parse ["asin"])) "should throw exception")
    (is (thrown? Exception (parse ["asin" "("])) "should throw exception")
    (is (thrown? Exception (parse ["asin" "(" "3"])) "should throw exception")
    (is (= (Math/asin 1) ((parse ["asin" "(" "x" ")"]))) "should parse asin(x)")))

(deftest parse-sinh-test
  (testing 
    "parse sinh"
    (is (thrown? Exception (parse ["sinh"])) "should throw exception")
    (is (thrown? Exception (parse ["sinh" "("])) "should throw exception")
    (is (thrown? Exception (parse ["sinh" "(" "3"])) "should throw exception")
    (is (= (Math/sinh 3) ((parse ["sinh" "(" "3" ")"]))) "should parse sinh(3)")
    (is (= (Math/sinh 1) ((parse ["sinh" "(" "x" ")"]))) "should parse sinh(x)")))

(deftest parse-cos-test
  (testing 
    "parse cos"
    (is (thrown? Exception (parse ["cos"])) "should throw exception")
    (is (thrown? Exception (parse ["cos" "("])) "should throw exception")
    (is (thrown? Exception (parse ["cos" "(" "3"])) "should throw exception")
    (is (= (Math/cos 5) ((parse ["cos" "(" "5" ")"]))) "should parse cos(5)")
    (is (= (Math/cos 1) ((parse ["cos" "(" "x" ")"]))) "should parse cos(x)")))

(deftest parse-acos-test
  (testing 
    "parse acos"
    (is (thrown? Exception (parse ["acos"])) "should throw exception")
    (is (thrown? Exception (parse ["acos" "("])) "should throw exception")
    (is (thrown? Exception (parse ["acos" "(" "3"])) "should throw exception")
    (is (= (Math/acos 1) ((parse ["acos" "(" "x" ")"]))) "should parse acos(x)")))

(deftest parse-cosh-test
  (testing 
    "parse cosh"
    (is (thrown? Exception (parse ["cosh"])) "should throw exception")
    (is (thrown? Exception (parse ["cosh" "("])) "should throw exception")
    (is (thrown? Exception (parse ["cosh" "(" "3"])) "should throw exception")
    (is (= (Math/cosh 5) ((parse ["cosh" "(" "5" ")"]))) "should parse cosh(5)")
    (is (= (Math/cosh 1) ((parse ["cosh" "(" "x" ")"]))) "should parse cosh(x)")))

(deftest parse-tan-test
  (testing 
    "parse tan"
    (is (thrown? Exception (parse ["tan"])) "should throw exception")
    (is (thrown? Exception (parse ["tan" "("])) "should throw exception")
    (is (thrown? Exception (parse ["tan" "(" "3"])) "should throw exception")
    (is (= (Math/tan 5) ((parse ["tan" "(" "5" ")"]))) "should parse tan(5)")
    (is (= (Math/tan 1) ((parse ["tan" "(" "x" ")"]))) "should parse tan(x)")))

(deftest parse-atan-test
  (testing 
    "parse atan"
    (is (thrown? Exception (parse ["atan"])) "should throw exception")
    (is (thrown? Exception (parse ["atan" "("])) "should throw exception")
    (is (thrown? Exception (parse ["atan" "(" "3"])) "should throw exception")
    (is (= (Math/atan 1) ((parse ["atan" "(" "x" ")"]))) "should parse atan(x)")))

(deftest parse-tanh-test
  (testing 
    "parse tanh"
    (is (thrown? Exception (parse ["tanh"])) "should throw exception")
    (is (thrown? Exception (parse ["tanh" "("])) "should throw exception")
    (is (thrown? Exception (parse ["tanh" "(" "3"])) "should throw exception")
    (is (= (Math/tanh 5) ((parse ["tanh" "(" "5" ")"]))) "should parse tanh(5)")
    (is (= (Math/tanh 1) ((parse ["tanh" "(" "x" ")"]))) "should parse tanh(x)")))

(deftest parse-log-test
  (testing 
    "parse log"
    (is (thrown? Exception (parse ["log"])) "should throw exception")
    (is (thrown? Exception (parse ["log" "("])) "should throw exception")
    (is (thrown? Exception (parse ["log" "(" "3"])) "should throw exception")
    (is (= (Math/log 5) ((parse ["log" "(" "5" ")"]))) "should parse log(5)")
    (is (= (Math/log 1) ((parse ["log" "(" "x" ")"]))) "should parse log(x)")))

(deftest parse-sqrt-test
  (testing 
    "parse sqrt"
    (is (thrown? Exception (parse ["sqrt"])) "should throw exception")
    (is (thrown? Exception (parse ["sqrt" "("])) "should throw exception")
    (is (thrown? Exception (parse ["sqrt" "(" "9"])) "should throw exception")
    (is (= (Math/sqrt 11) ((parse ["sqrt" "(" "11" ")"]))) "should parse sqrt(11)")
    (is (= (Math/sqrt 1) ((parse ["sqrt" "(" "x" ")"]))) "should parse sqrt(x)")))

(deftest parse-cbrt-test
  (testing 
    "parse cbrt"
    (is (thrown? Exception (parse ["cbrt"])) "should throw exception")
    (is (thrown? Exception (parse ["cbrt" "("])) "should throw exception")
    (is (thrown? Exception (parse ["cbrt" "(" "9"])) "should throw exception")
    (is (= (Math/cbrt 11) ((parse ["cbrt" "(" "11" ")"]))) "should parse cbrt(11)")
    (is (= (Math/cbrt 1) ((parse ["cbrt" "(" "x" ")"]))) "should parse cbrt(x)")))

(deftest parse-abs-test
  (testing 
    "parse abs"
    (is (thrown? Exception (parse ["abs"])) "should throw exception")
    (is (thrown? Exception (parse ["abs" "("])) "should throw exception")
    (is (thrown? Exception (parse ["abs" "(" "9"])) "should throw exception")
    (is (= 1.0 ((parse ["abs" "(" "-" "1" ")"]))) "should parse abs(-1)")
    (is (= 1 ((parse ["abs" "(" "-" "x" ")"]))) "should parse abs(-x)")))

(deftest parse-complex-unary-test
  (testing 
    "parse complex unary operations"
    (is (thrown? Exception (parse ["sin" "(" "cos" "(" "3" ")"])) "should throw exception")
    (is (= (Math/sin (Math/cos 3)) ((parse ["sin" "(" "cos" "(" "3" ")" ")"]))) "should parse sin(cos(3))")
    (is (= (Math/sin (Math/cos 1)) ((parse ["sin" "(" "cos" "(" "x" ")" ")"]))) "should parse sin(cos(x))")))

(deftest parse-value-with-parenthesis
  (testing 
    "parse value starting with parenthesis"
    (is (thrown? Exception (parse ["("])) "should throw exception")
    (is (thrown? Exception (parse ["(" "2"])) "should throw exception")
    (is (thrown? Exception (parse ["(" "x"])) "should throw exception")
    (is (= 2.0 ((parse ["(" "2" ")"]))) "should parse (2)")
    (is (= 1 ((parse ["(" "x" ")"]))) "should parse (x)")))

(deftest parse-unary-with-parenthesis
  (testing 
    "parse unary operations starting with parenthesis"
    (is (thrown? Exception (parse ["(" "sin" "("])) "should throw exception")
    (is (thrown? Exception (parse ["(" "sin" "(" "2"])) "should throw exception")
    (is (thrown? Exception (parse ["(" "sin" "(" "2" ")"])) "should throw exception")
    (is (= (Math/sin 2) ((parse ["(" "sin" "(" "2" ")" ")"]))) "should parse sin(2)")
    (is (= (Math/sin 1) ((parse ["(" "sin" "(" "x" ")" ")"]))) "should parse sin(x)")))

(deftest parse-complex-with-parenthesis
  (testing 
    "parse complex unary operations starting with parenthesis"
    (is (thrown? Exception (parse ["(" "sin" "(" "cos" "(" "2"])) "should throw exception")
    (is (thrown? Exception (parse ["(" "sin" "(" "cos" "(" "2" ")"])) "should throw exception")
    (is (thrown? Exception (parse ["(" "sin" "(" "cos" "(" "2" ")" ")"])) "should throw exception")
    (is (= (Math/sin (Math/cos 2)) ((parse ["(" "sin" "(" "cos" "(" "2" ")" ")" ")"]))) "should parse value")
    (is (= (Math/sin (Math/cos 1)) ((parse ["(" "sin" "(" "cos" "(" "x" ")" ")" ")"]))) "should parse value")))

(deftest precedences-test
  (testing 
    "precedences"
    (is (= 2.0 ((parse ["2"]))) "should parse 2")
    (is (= 2.0 ((parse ["(" "2" ")"]))) "should parse (2)")
    (is (= 9.0 ((parse ["6" "+" "3"]))) "should parse (2)+3")
    (is (= 9.0 ((parse ["(" "6" "+" "3" ")"]))) "should parse (2)+3")
    (is (= 9.0 ((parse ["3" "+" "(" "6" ")"]))) "should parse (6)+3")
    (is (= 9.0 ((parse ["(" "6" ")" "+" "3"]))) "should parse (6)+3")
    (is (= 8.0 ((parse ["6" "+" "3" "-" "1"]))) "should parse (2)+3")
    (is (= 5.0 ((parse (tokenize "2+3")))) "should parse (2)+3")
    (is (= 17.0 ((parse (tokenize "2+3*5")))) "should parse (2)+3")
    (is (= 11.0 ((parse (tokenize "2*3+5")))) "should parse (2)+3")
    (is (= 163.0 ((parse (tokenize "1+2*3^4")))) "should parse 1+2*3^4")))

(deftest e2e-test
  (testing 
    "e2e tests"
    (is (= 2.0 ((parse (tokenize "2")))) "should parse 2")
    (is (= 0.2 ((parse (tokenize ".2")))) "should parse .2")
    (is (= 1.2 ((parse (tokenize "1.2")))) "should parse 1.2")
    (is (= 1 ((parse (tokenize "x")))) "should parse x")
    (is (= (Math/E) ((parse (tokenize "e")))) "should parse e")
    (is (= (Math/E) ((parse (tokenize "E")))) "should parse E")
    (is (= (Math/PI) ((parse (tokenize "pi")))) "should parse pi")
    (is (= (Math/PI) ((parse (tokenize "PI")))) "should parse PI")
    (is (= 5.0 ((parse (tokenize "2+3")))) "should parse 2+3")
    (is (= 6.0 ((parse (tokenize "2*3")))) "should parse 2*3")
    (is (= 6.0 ((parse (tokenize "2*3")))) "should parse 2*3")
    (is (= 2.0 ((parse (tokenize "6/3")))) "should parse 6/3")
    (is (= 2.0 ((parse (tokenize "6-4")))) "should parse 6-4")
    (is (= 14.0 ((parse (tokenize "2+3*4")))) "should parse 2+3*4")
    (is (= 20.0 ((parse (tokenize "(2+3)*4")))) "should parse (2+3)*4")
    (is (= 10.0 ((parse (tokenize "2*3+4")))) "should parse 2*3+4")
    (is (= 14.0 ((parse (tokenize "2*(3+4)")))) "should parse 2*(3+4)")
    (is (= (+ 3 (/ (Math/sin 7) (Math/log 5))) ((parse (tokenize "3+sin(x+2*3)/log(x*2+3)")))) "should parse sin(x+2*3)/log(x*2+3)")
    (is (= (Math/sin (Math/pow 1 3)) ((parse (tokenize "sin(x^3)")))) "should parse sin(x^3)")
    (is (= (Math/pow (Math/sin 1) 3) ((parse (tokenize "sin(x)^3")))) "should parse sin(x)^3")))

(deftest set-x-tests
  (testing
    "set-unknown-x tests"
    (set-x 3)
    (is (= (Math/sin 3) ((parse (tokenize "sin(x)")))) "should parse sin(x) where x is 3")
    (set-x 2)
    (is (= (Math/sin 2) ((parse (tokenize "sin(x)")))) "should parse sin(x) where x is 2")
    (set-x 1)
    (is (= (Math/sin 1) ((parse (tokenize "sin(x)")))) "should parse sin(x) where x is 1")))














