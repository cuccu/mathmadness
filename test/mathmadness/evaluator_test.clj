(ns mathmadness.evaluator-test
  (:require [clojure.test :refer :all]
            [mathmadness.core :refer :all]))

(deftest to-func-test
  (testing 
    "to-func"
    (is (= (Math/sin 1) ((to-func "sin(1)"))) "should eval a function")
    (is (thrown? Exception ((to-func "abc"))) "should throw exception")))

(deftest to-val-test
  (testing 
    "to-val"
    (let [f (to-func "sin(x)")]
      (is (= (Math/sin 1) (to-val f 1)) "should eval a function"))))

(deftest to-vals-test
  (testing
    "to-vals"
    (let [f (to-func "sin(x)")
          coll [1 2 3]
          expected [(Math/sin 1) (Math/sin 2) (Math/sin 3)]]
      (is (= expected (to-vals f coll)) "should apply function to collection"))))

(deftest to-integral-test
  (testing 
    "to-integral"
    (let [f (to-func "sin(x)")]
      (is (= 1.999992324664907 (to-integral f 0 3.1457)) "should eval integral"))))