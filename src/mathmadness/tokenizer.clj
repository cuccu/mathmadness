(ns mathmadness.tokenizer
  (:use mathmadness.utils))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TOKENS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn ^:private token?
  [str tkn]
  (and
    (not-nil? str)
    (.startsWith str tkn)))

(defn x? [str] (token? str "x"))

(defn e? [str] (token? str "e"))

(defn pi? [str] (token? str "pi"))

(defn plus? [str] (token? str "+"))

(defn minus? [str] (token? str "-"))

(defn times? [str] (token? str "*"))

(defn divide? [str] (token? str "/"))

(defn pow? [str] (token? str "^"))

(defn open? [str] (token? str "("))

(defn close? [str] (token? str ")"))

(defn sin? [str] (token? str "sin"))

(defn asin? [str] (token? str "asin"))

(defn sinh? [str] (token? str "sinh"))

(defn cos? [str] (token? str "cos"))

(defn acos? [str] (token? str "acos"))

(defn cosh? [str] (token? str "cosh"))

(defn tan? [str] (token? str "tan"))

(defn atan? [str] (token? str "atan"))

(defn tanh? [str] (token? str "tanh"))

(defn log? [str] (token? str "log"))

(defn sqrt? [str] (token? str "sqrt"))
  
(defn cbrt? [str] (token? str "cbrt"))

(defn abs? [str] (token? str "abs"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; NUMBERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def numbers [\1 \2 \3 \4 \5 \6 \7 \8 \9 \0 \.])

(defn numb? 
  [str] 
  (in? numbers (first str)))

(defn numbs 
  [str] 
  (clojure.string/join 
    (when 
      (and
        (seq str)
        (numb? str))
      (into [] (concat [(sfirst str)] (numbs (srest str)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TOKENIZER
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn ^:private tokenize-pvt
  [user-str previous] 
  (let [s (rmspcs user-str)]    
    (when
      (seq s)
      (into[] 
           (cond                
             (sqrt? s)
             (concat ["sqrt"] (tokenize-pvt (.substring s 4) nil)) 
             (cbrt? s)
             (concat ["cbrt"] (tokenize-pvt (.substring s 4) nil))                 
             (asin? s) 
             (concat ["asin"] (tokenize-pvt (.substring s 4) nil)) 
             (sinh? s) 
             (concat ["sinh"] (tokenize-pvt (.substring s 4) nil)) 
             (sin? s) 
             (concat ["sin"] (tokenize-pvt (.substring s 3) nil))
             (acos? s) 
             (concat ["acos"] (tokenize-pvt (.substring s 4) nil))
             (cosh? s) 
             (concat ["cosh"] (tokenize-pvt (.substring s 4) nil))
             (cos? s) 
             (concat ["cos"] (tokenize-pvt (.substring s 3) nil))
             (tanh? s) 
             (concat ["tanh"] (tokenize-pvt (.substring s 4) nil))
             (atan? s) 
             (concat ["atan"] (tokenize-pvt (.substring s 4) nil))  
             (tan? s) 
             (concat ["tan"] (tokenize-pvt (.substring s 3) nil))                             
             (log? s) 
             (concat ["log"] (tokenize-pvt (.substring s 3) nil)) 
             (abs? s) 
             (concat ["abs"] (tokenize-pvt (.substring s 3) nil)) 
             (x? s) 
             (concat ["x"] (tokenize-pvt (.substring s 1) nil)) 
             (e? s) 
             (concat ["e"] (tokenize-pvt (.substring s 1) nil)) 
             (pi? s) 
             (concat ["pi"] (tokenize-pvt (.substring s 2) nil)) 
             (plus? s) 
             (concat [")" ")" ")" "+" "(" "(" "("] (tokenize-pvt (.substring s 1) "+")) 
             (and (minus? s) (nil? previous))
             (concat [")" ")" ")" "-" "(" "(" "("] (tokenize-pvt (.substring s 1) "-")) 
             (and (minus? s) (not (nil? previous)))
             (concat ["-"] (tokenize-pvt (.substring s 1) "-")) 
             (times? s) 
             (concat [")" ")" "*" "(" "("] (tokenize-pvt (.substring s 1) "*")) 
             (divide? s) 
             (concat [")" ")" "/" "(" "("] (tokenize-pvt (.substring s 1) "/")) 
             (pow? s) 
             (concat [")" "^" "("] (tokenize-pvt (.substring s 1) "^")) 
             (open? s) 
             (concat ["(" "(" "(" "("] (tokenize-pvt (.substring s 1) "(")) 
             (close? s) 
             (concat [")" ")" ")" ")"] (tokenize-pvt (.substring s 1) nil)) 
             (Double/parseDouble (numbs s))
             (let [n (numbs s)] (concat [n] (tokenize-pvt (.substring s (count n)) nil)))                
             :else 
             (throw (Exception. (format "Unknown token: %s" s))))))))

(defn tokenize
  [user-str]
  (let [s (-> user-str (.toLowerCase) (tokenize-pvt ""))]
    (concat ["(" "(" "(" "("] s [")" ")" ")" ")"])))
