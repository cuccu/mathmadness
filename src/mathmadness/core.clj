(ns mathmadness.core
  (:use mathmadness.parser)
  (:use mathmadness.tokenizer))

(defn to-func
  [user-str]
  (-> user-str tokenize parse))

(defn to-val
  [func x]
  (do
    (set-x x)
    (func)))

(defn to-vals
  [func coll]
  (map #(to-val func %) coll))

(defn to-integral
  [func from to]
  (let [delta 0.001]
    (reduce
      (fn [a b] (+ a (* delta b)))
      (to-vals func (range from to delta)))))
