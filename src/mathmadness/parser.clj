(ns mathmadness.parser
  (:use mathmadness.tokenizer)
  (:use mathmadness.utils))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Unknown - x
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def unknown (atom 1))

(defn set-x 
  [v] 
  (swap! unknown (fn [a] v)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Checks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn value?
  [tokens]
  (not-empty? (numbs (first tokens))))

(defn ope? 
  [tokens] 
  (in? ["+" "-" "*" "/" "^"] (first tokens)))

(defn unary? 
  [tokens] 
  (in? 
    ["sin" "asin" "sinh" 
     "cos" "acos" "cosh" 
     "tan" "atan" "tanh" 
     "log" "sqrt" "cbrt"
     "abs"] 
    (first tokens)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Constant operations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn ope-x
  []
  (fn [] @unknown))

(defn ope-e
  []
  (fn [] (Math/E)))

(defn ope-pi
  []
  (fn [] (Math/PI)))

(defn ope-value
  [val]
  (fn [] val))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Unary operations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn ope-sin
  [f]
  (fn [] (Math/sin (f))))

(defn ope-asin
  [f]
  (fn [] (Math/asin (f))))

(defn ope-sinh
  [f]
  (fn [] (Math/sinh (f))))

(defn ope-cos
  [f]
  (fn [] (Math/cos (f))))

(defn ope-acos
  [f]
  (fn [] (Math/acos (f))))

(defn ope-cosh
  [f]
  (fn [] (Math/cosh (f))))

(defn ope-tan
  [f]
  (fn [] (Math/tan (f))))

(defn ope-atan
  [f]
  (fn [] (Math/atan (f))))

(defn ope-tanh
  [f]
  (fn [] (Math/tanh (f))))

(defn ope-log
  [f]
  (fn [] (Math/log (f))))

(defn ope-sqrt
  [f]
  (fn [] (Math/sqrt (f))))

(defn ope-cbrt
  [f]
  (fn [] (Math/cbrt (f))))

(defn ope-abs
  [f]
  (fn [] (Math/abs (f))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Binary operations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn ope-add
  [f1 f2]
  (fn [] (+ (f1) (f2))))

(defn ope-sub
  [f1 f2]
  (fn [] (- (f1) (f2))))

(defn ope-mult
  [f1 f2]
  (fn [] (* (f1) (f2))))

(defn ope-division
  [f1 f2]
  (fn [] 
    (let [r2 (f2)]
      (if (zero? r2)
        (Double/NaN)
        (/ (f1) (f2))))))

(defn ope-pow
  [f1 f2]
  (fn [] (Math/pow (f1) (f2))))

(defn ope-negative
  [f1]
  (fn [] (- (f1))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Factories
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
(defn ope
  [tkn f1 f2]
  (cond
    (plus? tkn)   (ope-add f1 f2)
    (minus? tkn)  (ope-sub f1 f2)
    (times? tkn)  (ope-mult f1 f2)
    (divide? tkn) (ope-division f1 f2)
    (pow? tkn)    (ope-pow f1 f2)))

(defn unary
  [tkn f1]
  (cond    
    (asin? tkn) (ope-asin f1)
    (sinh? tkn) (ope-sinh f1)
    (sin? tkn)  (ope-sin f1)    
    (acos? tkn) (ope-acos f1)
    (cosh? tkn) (ope-cosh f1)
    (cos? tkn)  (ope-cos f1)    
    (atan? tkn) (ope-atan f1)
    (tanh? tkn) (ope-tanh f1)
    (tan? tkn)  (ope-tan f1)
    (log? tkn)  (ope-log f1)
    (sqrt? tkn) (ope-sqrt f1)
    (cbrt? tkn) (ope-cbrt f1)
    (abs? tkn) (ope-abs f1)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Errors
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
(defn ^:private parse-err
  [expected found]
  (format "Parse error: expected %s but found %s" expected found))

(defn ^:private unexpected-token
  [token]
  (format "Parse error: unexpected token: %s" token))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PARSER:
;; 
;; expr:
;;   value:  number    | x
;;   unary:  sin(expr) | cos(expr) | log(expr) | sqrt(expr)
;;   binary: expr+expr | expr-expr | expr*expr | expr/expr
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(declare pvt-parse)

(defn ^:private apply-next-operations
  [f1 cur-tkns]
  (if 
    (ope? (rest cur-tkns))
    (let [f2 (pvt-parse {:tkn (drop 2 cur-tkns)})]
      {:fun (ope (second cur-tkns) f1 (:fun f2)) :tkn (:tkn f2)})
    {:fun f1 :tkn (rest cur-tkns)}))

(defn ^:private pvt-parse
  [map]   
  (let [cur-fun (:fun map) 
        cur-tkns (:tkn map)
        fst-tkn (first cur-tkns)]
    (when
      (seq cur-tkns)
      (cond              
        
        ;; parseNegative
        (minus? fst-tkn)
        (let [f (pvt-parse {:tkn (rest cur-tkns)})]
          {:fun (ope-negative (:fun f)) :tkn (:tkn f)})
        
        ;; parseX
        (x? fst-tkn)
        (apply-next-operations (ope-x) cur-tkns)
        
        ;; parseE
        (e? fst-tkn)
        (apply-next-operations (ope-e) cur-tkns)
        
        ;; parsePI
        (pi? fst-tkn)
        (apply-next-operations (ope-pi) cur-tkns)
        
        ;; parseValue
        (value? cur-tkns)
        (apply-next-operations (ope-value (Double/parseDouble fst-tkn)) cur-tkns)
        
        ;; parenthesis
        (open? fst-tkn)
        (let [f1 (pvt-parse {:tkn (rest cur-tkns)})]
          (if-not 
            (close? (first (:tkn f1)))
            (throw (Exception. (parse-err ")" (first (:tkn f1))))))
          (if 
            (ope? (rest (:tkn f1)))
            (let [f2 (pvt-parse {:tkn (drop 2 (:tkn f1))})]
              {:fun (ope (second (:tkn f1)) (:fun f1) (:fun f2)) :tkn (:tkn f2)})
            {:fun (:fun f1) :tkn (rest (:tkn f1))}))                                         

        ;; parseUnary
        (unary? cur-tkns)
        (if-not 
          (open? (second cur-tkns)) 
          (throw (Exception. (parse-err "(" (second cur-tkns)))) 
          (let [res (pvt-parse {:tkn (drop 2 cur-tkns)})]
            (if-not 
              (close? (first (:tkn res)))
              (throw (Exception. (parse-err ")" (first (:tkn res)))))
              {:fun (unary (first cur-tkns) (:fun res)) 
               :tkn (rest (:tkn res))})))
        
        :else 
        (throw (Exception. (unexpected-token fst-tkn)))))))

(defn parse
  [tokens]
  (:fun (pvt-parse {:tkn tokens})))









