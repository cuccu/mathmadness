(ns mathmadness.utils)

(def not-empty? (comp not empty?))

(def not-nil? (comp not nil?))

(defn sfirst 
  [str] 
  (.substring str 0 1))

(defn srest 
  [str] 
  (.substring str 1))

(defn in? 
  [coll val] 
  (reduce 
    #(or %1 (= val %2)) 
    false 
    coll))

(defn rmspcs 
  [str] 
  (when (not-nil? str)
    (.replaceAll str " " "")))